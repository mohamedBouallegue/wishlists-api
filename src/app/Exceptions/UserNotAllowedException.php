<?php

namespace App\Exceptions;

/**
 * Class UserNotAllowedException
 *
 * @package App\Exceptions
 */
class UserNotAllowedException extends \Exception
{
}
