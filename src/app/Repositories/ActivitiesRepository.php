<?php
namespace App\Repositories;

use App\Models\Activity;

/**
 * Class ActivitiesRepository
 *
 * @package App\Repositories
 */
class ActivitiesRepository
{

    /**
     * @author Mohamed Bouallegue <mohamed.bouallegue@tattoodo.com>
     *
     * @param int $id
     *
     * @return mixed
     */
    public function findByIdOrFail(int $id)
    {
        $activity = Activity::findOrFail($id);

        return $activity;
    }
}
