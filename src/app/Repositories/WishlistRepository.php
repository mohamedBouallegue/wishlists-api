<?php
namespace App\Repositories;

use App\Models\Activity;
use App\Models\Wishlist;

/**
 * Class WishlistRepository
 *
 * @package App\Repositories
 */
class WishlistRepository
{

    /**
     * @author Mohamed Bouallegue <mohamed.bouallegue@tattoodo.com>
     *
     * @param int $id
     *
     * @return mixed
     */
    public function getAllWishlistActivities(int $id)
    {
        $wishlist   = Wishlist::findOrFail($id);
        $activities = $wishlist->activities;

        return $activities;
    }


    /**
     * @author Mohamed Bouallegue <mohamed.bouallegue@tattoodo.com>
     *
     * @param int $id
     *
     * @return mixed
     */
    public function findByIdOrFail(int $id)
    {
        $wishlist = Wishlist::findOrFail($id);

        return $wishlist;
    }


    /**
     * @author Mohamed Bouallegue <mohamed.bouallegue@tattoodo.com>
     *
     * @param Wishlist $wishlist
     * @param Activity $activity
     *
     * @return bool
     */
    public function addActivityToWishlist(Wishlist $wishlist, Activity $activity)
    {
        $wishlist->activities()
                 ->syncWithoutDetaching([$activity->id]);

        return true;
    }


    /**
     * @author Mohamed Bouallegue <mohamed.bouallegue@tattoodo.com>
     *
     * @param Wishlist $wishlist
     * @param int      $activityId
     *
     * @return bool
     */
    public function removeActivityFromWishlist(Wishlist $wishlist, int $activityId)
    {
        $wishlist->activities()
                 ->detach([$activityId]);

        return true;
    }
}
