<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Wishlist
 *
 * @package App\Models
 */
class Wishlist extends Model
{
    /**
     * @author Mohamed Bouallegue <mohamed.bouallegue@tattoodo.com>
     * @return \Illuminate\Database\Eloquent\Relations\MorphToMany
     */
    public function activities()
    {
        return $this->morphedByMany(Activity::class, 'listable');
    }
}
