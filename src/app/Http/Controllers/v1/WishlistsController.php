<?php

namespace App\Http\Controllers\v1;

use App\Exceptions\UserNotAllowedException;
use App\Repositories\ActivitiesRepository;
use App\Repositories\WishlistRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

/**
 * Class WishlistsController
 *
 * @package App\Http\Controllers\v1
 */
class WishlistsController extends Controller
{
    protected $activitiesRepository;
    /**
     * @var WishlistRepository
     */
    private $wishlistRepository;


    /**
     * WishlistsController constructor.
     */
    public function __construct(WishlistRepository $wishlistRepository, ActivitiesRepository $activitiesRepository)
    {
        $this->wishlistRepository   = $wishlistRepository;
        $this->activitiesRepository = $activitiesRepository;
    }


    /**
     * @author Mohamed Bouallegue <mohamed.bouallegue@tattoodo.com>
     *
     * @param int $id
     *
     * @return mixed
     * @throws UserNotAllowedException
     */
    public function showActivities(int $id)
    {
        $currentUser = Auth::user();
        $wishlist    = $this->wishlistRepository->findByIdOrFail($id);

        if ($currentUser->id !== $wishlist->id) {
            throw new UserNotAllowedException();
        }

        $activities = $this->wishlistRepository->getAllWishlistActivities($id);

        return $activities;
    }


    /**
     * @author Mohamed Bouallegue <mohamed.bouallegue@tattoodo.com>
     *
     * @param int $wishlistId
     * @param int $activityId
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws UserNotAllowedException
     */
    public function addActivity(int $wishlistId, int $activityId)
    {
        $wishlist    = $this->wishlistRepository->findByIdOrFail($wishlistId);
        $currentUser = Auth::user();

        if ($currentUser->id !== $wishlist->id) {
            throw new UserNotAllowedException();
        }
        $activity = $this->activitiesRepository->findByIdOrFail($activityId);

        $this->wishlistRepository->addActivityToWishlist($wishlist, $activity);

        return response()->json(
            [
                'status'  => 'success',
                'message' => 'Activity added succesfully',
            ]
        );
    }


    /**
     * @author Mohamed Bouallegue <mohamed.bouallegue@tattoodo.com>
     *
     * @param int $wishlistId
     * @param int $activityId
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws UserNotAllowedException
     */
    public function removeActivity(int $wishlistId, int $activityId)
    {
        $wishlist    = $this->wishlistRepository->findByIdOrFail($wishlistId);
        $currentUser = Auth::user();

        if ($currentUser->id !== $wishlist->id) {
            throw new UserNotAllowedException();
        }

        $this->wishlistRepository->removeActivityFromWishlist($wishlist, $activityId);

        return response()->json(
            [
                'status'  => 'success',
                'message' => 'Activity removed succesfully',
            ]
        );
    }
}
