<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['namespace' => 'v1', 'prefix' => 'v1', 'middleware' => ['auth:api']], function() {
    Route::get('/wishlists/{id}/activities', 'WishlistsController@showActivities');
    Route::post('/wishlists/{wishlist_id}/activities/{activity_id}', 'WishlistsController@addActivity');
    Route::delete('/wishlists/{wishlist_id}/activities/{activity_id}', 'WishlistsController@removeActivity');
});
