<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateListableTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'listables',
            function (Blueprint $table) {
                $table->increments('id');
                $table->integer('wishlist_id')
                      ->unsigned();
                $table->morphs('listable');
                $table->timestamps();

                $table->foreign('wishlist_id')
                      ->references('id')
                      ->on('wishlists');
            }
        );
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('listable');
    }
}
