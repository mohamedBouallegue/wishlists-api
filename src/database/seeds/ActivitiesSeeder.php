<?php

use Illuminate\Database\Seeder;

class ActivitiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('activities')
          ->insert(
              [
                  [
                      'title' => 'Berlin tour',
                  ],
                  [
                      'title' => 'Copenhagen tour',
                  ],
                  [
                      'title' => 'Paris tour',
                  ],
                  [
                      'title' => 'New York tour',
                  ],
              ]
          );
    }
}
