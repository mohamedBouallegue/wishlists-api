<?php

use Illuminate\Database\Seeder;

class WishlistSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('wishlists')
          ->insert(
              [
                  [
                      'title'   => 'Summer 2017',
                      'user_id' => 1,
                  ],
              ]
          );
    }
}
