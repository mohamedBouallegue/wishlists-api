<?php

use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')
          ->insert(
              [
                  'name'      => 'mohamed',
                  'email'     => 'mohamed.bouallegue.tn@gmail.com',
                  'password'  => encrypt('secret'),
                  'api_token' => str_random(60),
              ]
          );
    }
}
