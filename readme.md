## Wishlists-api

### Setup with docker-compose
Make sure you don't have any service running on ports 80 and 3306 then run these 4 commands in the project's root directory.

`docker-compose up -d`

`docker exec -it gyg_php_1 bash`

`php /usr/local/nginx/html/artisan migrate`

`php /usr/local/nginx/html/artisan db:seed`

The site should be then on http://localhost

These are the available endpoints:
* `POST localhost/api/v1/wishlists/1/activities/1` add activity to wishlist.
* `DELETE localhost/api/v1/wishlists/1/activities/1` remove activity from wishlist.
* `GET localhost/api/v1/wishlists/1/activities` list activities in a wishlist.

All the requests needs to include a token `Authorization : Bearer $token`

All the stories are implemented including security with tokens. However it is not the best way to implement them. 

The next step to improve the current project would be:
* Handling exceptions correctly and returning the correct status codes.
* Add a service layer for managing wishlists.
* Reorganize the folders structure.
* Add interfaces for services and repository and bind them to the implementation.
* Improve response format (Fractal can be a good option)
* Write unit and integration tests
* ...




